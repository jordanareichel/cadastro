<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Administrador;

/**
 * AdministradorSearch represents the model behind the search form about `app\models\Administrador`.
 */
class AdministradorSearch extends Administrador
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adm_id'], 'integer'],
            [['adm_proprietario', 'adm_condominio'], 'safe'],
            [['adm_lote'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Administrador::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'adm_id' => $this->adm_id,
            'adm_lote' => $this->adm_lote,
        ]);

        $query->andFilterWhere(['like', 'adm_proprietario', $this->adm_proprietario])
            ->andFilterWhere(['like', 'adm_condominio', $this->adm_condominio]);

        return $dataProvider;
    }
}
