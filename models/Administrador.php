<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "administrador".
 *
 * @property integer $adm_id
 * @property string $adm_proprietario
 * @property string $adm_condominio
 * @property string $adm_lote
 */
class Administrador extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'administrador';
    }
     public function afterFind()
    {
        if (!empty($this->name)) {

            if($this->name == 1 ){
                $this->name = 'A';
            }

            if($this->name == 2 ){
                $this->name = 'B';
            }

            if($this->name == 3 ){
                $this->name = 'C';
            }
           
        }

        return parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adm_proprietario', 'adm_condominio', 'adm_lote'], 'required'],
            [['adm_condominio'], 'string'],
            [['adm_lote'], 'string'],
            [['adm_proprietario'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'adm_id' => 'ID',
            'adm_proprietario' => 'Proprietario',
            'adm_condominio' => 'Condominio',
            'adm_lote' => 'Apartamento',
        ];
    }

    public function adm() {

        $adm = [

                '1' => 'A',
                '2' => 'B',
                '3' => 'C',
                '4' => 'D'

        ];
        return $adm;
    }

    public function lnc () {

        $lnc = [

                '1-5' => '1-5',
                '2' => '6-11',
                '3' => '12-17'


                ];
                return $lnc;
    }
}
