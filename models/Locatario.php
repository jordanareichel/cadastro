<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "locatario".
 *
 * @property integer $loc_id
 * @property string $loc_nome
 * @property string $loc_endereco
 * @property string $loc_telefone
 * @property string $loc_apartamento
 */
class Locatario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locatario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loc_nome', 'loc_endereco', 'loc_telefone', 'loc_apartamento'], 'required'],
            [['loc_telefone'], 'integer'],
            [['loc_nome', 'loc_endereco'], 'string', 'max' => 200],
            [['loc_apartamento'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'loc_id' => 'Loc ID',
            'loc_nome' => 'Nome',
            'loc_rg' => 'RG',
            'loc_endereco' => 'CPF',
            'loc_telefone' => 'Telefone',
            'loc_apartamento' => 'Imovel',
        ];
    }
}
