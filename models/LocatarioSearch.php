<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Locatario;

/**
 * LocatarioSearch represents the model behind the search form about `app\models\Locatario`.
 */
class LocatarioSearch extends Locatario
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loc_id', 'loc_telefone'], 'integer'],
            [['loc_nome', 'loc_endereco', 'loc_apartamento'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Locatario::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'loc_id' => $this->loc_id,
            'loc_telefone' => $this->loc_telefone,
        ]);

        $query->andFilterWhere(['like', 'loc_nome', $this->loc_nome])
            ->andFilterWhere(['like', 'loc_endereco', $this->loc_endereco])
            ->andFilterWhere(['like', 'loc_apartamento', $this->loc_apartamento]);

        return $dataProvider;
    }
}
