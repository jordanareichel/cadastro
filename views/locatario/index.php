<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LocatarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cadastro de Locatarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locatario-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Novo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'loc_id',
            'loc_nome',
            'loc_endereco',
            'loc_rg',
            'loc_telefone',
            'loc_apartamento',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
