<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Locatario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="locatario-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
    <div class="col-md-4 col-sm-12 col-xs-12">	
    <?= $form->field($model, 'loc_nome')->textInput(['maxlength' => true]) ?>
    </div>	
    <div class="col-md-4 col-sm-12 col-xs-12">
    <?= $form->field($model, 'loc_endereco')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
    <?= $form->field($model, 'loc_rg')->textInput(['maxlength' => true]) ?>
    </div>
    
  	<div class="col-md-4 col-sm-12 col-xs-12">
    <?= $form->field($model, 'loc_telefone')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
    <?= $form->field($model, 'loc_apartamento')->textInput(['maxlength' => true]) ?>
    </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Salvar' : 'Salvar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
