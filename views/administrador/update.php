<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Administrador */

$this->title = 'Editar: ' . $model->adm_id;
$this->params['breadcrumbs'][] = ['label' => 'Administradors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->adm_id, 'url' => ['view', 'id' => $model->adm_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="">
  <div class="page-title">
    <div class="title_left">
      <h3><?= $this->title ?></h3> <br>
    </div>
    
  </div>

  <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
			    <?= $this->render('_form', [
			        'model' => $model,
			    ]) ?>
			   </div>
			</div>
		</div>