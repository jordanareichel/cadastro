<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Administrador */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="administrador-form">

    <?php $form = ActiveForm::begin(); ?>
      <div class="row">
     <div class="col-md-4 col-sm-12 col-xs-12">
    <?= $form->field($model, 'adm_proprietario')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
    <?= $form->field($model, 'adm_condominio')->dropDownList(['A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D',],['class' => 'form-control' , 'prompt' => 'Selecione']) ?>
    </div>
     <div class="col-md-4 col-sm-12 col-xs-12">
    <?= $form->field($model, 'adm_lote')->dropDownList(['1-5' => '1-5', '6-10' => '6-10', '11-16' => '11-16', '17-22' => '17-22',],['class' => 'form-control' , 'prompt' => 'Selecione'])?>
    </div>
    </div>
     <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                    <hr>
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Salvar' : 'Salvar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    </div>
                  </div>

    <?php ActiveForm::end(); ?>

</div>
