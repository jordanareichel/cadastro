<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdministradorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="administrador-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div id="collapseTwo" class="panel-collapse collapse" style="height: 0px;">
  <div class="panel-body">
    <div class="row">

    <?= $form->field($model, 'adm_id') ?>

    <?= $form->field($model, 'adm_proprietario') ?>
    
    <?= $form->field($model, 'adm_condominio') ?>

    <?= $form->field($model, 'adm_lote') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
