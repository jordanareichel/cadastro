<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Administrador */

$this->title = 'Usuario';
$this->params['breadcrumbs'][] = ['label' => 'Administradors', 'url' => ['index']];

?>
<div class="content-header">
  <h2 class="content-header-title"><?= $this->title ?></h2>

</div> 

  <?= $this->render('_form', [
      'model' => $model,
  ]) ?>
